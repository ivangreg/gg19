# Proteome-wide prediction of phosphorylation in human and rodents

by Ivan V. Gregoretti and Florian Gnad.



## Features and labels for machine learning

The features are based on Uniprot IDs and and annotations plus Ensembl evolutionary comparisons plus SPIDER3 secondary structure prediction. The features is stored in tabular form with the last two columns showing the number of literature reports (papers) of small-scale studies (`phos_lt`) and large-scale studies (`phos_ms`).

These files are

    uniprot_20180621
    ├── SEQBLOSP3H3.h5
    ├── SEQBLOSP3M3.h5
    └── SEQBLOSP3R3.h5

and they correspond to Human, Mouse and Rat respectively.

>Note: we intended to make these feature files avialable in Parquet format but that made the files about 2.5 times the size of their H5 counterparts.



## Phosphorylation prediction

Phosphorylation prediction is broken down into nine files: three species times three aminoacids. For every aminoacid, 31 stochastic predictions were computed. Within each file, every aminoacid is unambiguously and systematically identified and the last three columns show the median, mean and standard deviation of the 31 modeling rounds. The mean is what we refer to as the Phosphosite Assignment Score.

The files are

    phospho_predictions
    ├── PRBPHOSH1.parquet
    ├── PRBPHOSM1.parquet
    ├── PRBPHOSR1.parquet
    ├── PRBPHOTH1.parquet
    ├── PRBPHOTM1.parquet
    ├── PRBPHOTR1.parquet
    ├── PRBPHOYH1.parquet
    ├── PRBPHOYM1.parquet
    └── PRBPHOYR1.parquet

The first three files are predictions for Serine, the next three are prediction for Threonine and the last three are predictions for Tyrosine.


